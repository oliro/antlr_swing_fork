tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr 
        | ^(PRINT e=expr){drukuj ($e.text + " = " + $e.out.toString());}
        | ^(VAR i1=ID) {createVar($i1.text);}
        | ^(PODST i1=ID   e2=expr) {setVar($i1.text, $e2.out);})* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = modulo($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);}
        | INT                      {$out = getInt($INT.text);}
        | ID                       {$out = getVar($ID.text);}
        ;
