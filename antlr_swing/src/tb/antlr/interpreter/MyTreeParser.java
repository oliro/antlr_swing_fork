package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	GlobalSymbols globalSymbols = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer divide(Integer a, Integer b) throws ArithmeticException {
		if (b == 0) throw new ArithmeticException("Próba dzielenia przez 0.");
		return a / b;
	}
	
	protected Integer modulo(Integer a, Integer b) throws ArithmeticException {
		if (b == 0) throw new ArithmeticException("Próba dzielenia przez 0.");
		return a % b;
	}
	
	protected Integer power(Integer a, Integer b) {
		return (int)Math.pow(a, b);
	}
	
	protected void createVar(String name) {
		globalSymbols.newSymbol(name);
	}
	
	protected Integer getVar(String name) {
		return globalSymbols.getSymbol(name);
	}
	
	protected void setVar(String name, Integer value) {
		globalSymbols.setSymbol(name, value);
	}
}
